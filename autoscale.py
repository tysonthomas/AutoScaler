#!/usr/bin/env python
import logging
import sys
import os
import argparse
import ConfigParser
import datetime
import time
import requests

try:
    import json
except ImportError:
    import simplejson as json

try:
    from libcloud.compute.types import Provider
    from libcloud.compute.providers import get_driver
    _ = Provider.GCE
except:
    print("GCE inventory script requires libcloud >= 0.13")
    sys.exit(1)

logger = logging.getLogger(__name__)


class MesosReporter():
    def __init__(self, mesos_url):
        self.mesos_url = mesos_url.rstrip('/')
        stats_url = ''.join([self.mesos_url, '/metrics/snapshot'])
        logger.info('URL: %s', stats_url)
        self._state = requests.get(stats_url).json()

    @property
    def state(self):
        return self._state


class MesosDecider():
    def __init__(self, thresholds):
        self.thresholds = thresholds

    def should_scale(self, cluster):
        increment = 1
        decrement = -1

        cpus_free = cluster.state['master/cpus_total'] - cluster.state['master/cpus_used']
        disk_free = cluster.state['master/disk_total'] - cluster.state['master/disk_used']
        mem_free = cluster.state['master/mem_total'] - cluster.state['master/mem_used']
        logger.info('State: %s', dict(cpus_free=cpus_free, disk_free=disk_free, mem_free=mem_free))
        logger.info('Thresholds: %s', self.thresholds)

        if   (('cpus' in self.thresholds and cpus_free < self.thresholds['cpus']['lower']) or
              ('disk' in self.thresholds and disk_free < self.thresholds['disk']['lower']) or
              ('mem' in self.thresholds and mem_free < self.thresholds['mem']['lower'])):
            scale_by = increment
        elif (('cpus' in self.thresholds and cpus_free > self.thresholds['cpus']['upper']) or
              ('disk' in self.thresholds and disk_free > self.thresholds['disk']['upper']) or
              ('mem' in self.thresholds and mem_free > self.thresholds['mem']['upper'])):
            scale_by = decrement
        else:
            scale_by = 0

        logger.info('Should scale by %s', scale_by)
        return scale_by

#Get the info about the state of Mesos Slaves
class MesosSlaveReporter():
    def __init__(self, mesos_url):
        self.mesos_url = mesos_url.rstrip('/')
        stats_url = ''.join([self.mesos_url, '/state'])
        logger.info('URL: %s', stats_url)
        self._state = requests.get(stats_url).json()

    @property
    def state(self):
        return self._state

#List out the Slave Instance with less than the max utlization
class MesosSlaveDecider():
    def __init__(self, cpu_utlization = 1):
        self.cpu_utlization = cpu_utlization

    def slave_info(self, cluster):
        cpu_utlization = self.cpu_utlization
        slave_info = None

        slaves = cluster.state['slaves']
    	for slave in slaves:
            logger.info(' Slave Hostname: %s, CPU: %s', slave['hostname'], slave['used_resources']['cpus'])
            if slave['used_resources']['cpus'] < cpu_utlization:
                slave_info = slave['hostname'].split('.')
                slave_info = slave_info[0]
                logger.info(' Slave Name: %s', slave_info)
                logger.info(' Slave CPU Utilization: %s', slave['used_resources']['cpus'])
                break

        return slave_info

#Autoscale group for aws cloud
class AwsAsgScaler():
    def __init__(self, region, asg_name, min_instances=1, max_instances=None, 
                 aws_access_key_id=None, aws_secret_access_key=None):
        self.region = region
        self.asg_name = asg_name
        self.min_instances = min_instances
        self.max_instances = max_instances
        self.aws_access_key_id = aws_access_key_id
        self.aws_secret_access_key = aws_secret_access_key

    def _get_connection(self):
        if self.aws_access_key_id and self.aws_secret_access_key:
            return boto.ec2.autoscale.connect_to_region(
                self.region, 
                aws_access_key_id=self.aws_access_key_id,
                aws_secret_access_key=self.aws_secret_access_key)
        else:
            return boto.ec2.autoscale.connect_to_region(self.region)

    def scale(self, delta):
        c = self._get_connection()
        current_count = c.get_all_groups(names=[self.asg_name])[0].desired_capacity
        logger.info("Current scale: %s", current_count)
        new_count = current_count + delta
        
        if self.min_instances and new_count < self.min_instances:
            new_count = self.min_instances
        elif self.max_instances and new_count > self.max_instances:
            new_count = self.max_instances

        if new_count != current_count:
            logger.info("Scaling to %s", new_count)
            c.set_desired_capacity(self.asg_name, new_count)

#Autoscale Service for GCE Cloud
class GceScaleService():
    def __init__(self, project_id, datacenter='us-central1-f', service_account=None, pem_key=None,
            min_instances=1, max_instances=None):
        self.datacenter = datacenter
        self.project_id = project_id
        self.service_account = service_account
        self.pem_key = pem_key

    def _get_connection(self):
        if self.service_account and self.pem_key:
            return get_driver(Provider.GCE)(self.service_account, self.pem_key, datacenter = self.datacenter, project = self.project_id)
        else:
            return get_driver(Provider.GCE)('', '', datacenter = self.datacenter, project = self.project_id)

    #TO-DO : increase the scope to include multiple system scale down using node_name as array
    #Scale Down service for GCE: Input the Node Name and the service removes the instance

    def scale_down(self, node_name):
        gce = self._get_connection()
        
        start_time = datetime.datetime.now()
        logger.info(' Compute Delete Node start time: %s' % str(start_time))

	    # Use ex_destroy_multiple_nodes to destroy nodes
        del_nodes = []

        #	all_nodes = gce.list_nodes(ex_zone='all')
        #	display('Nodes:', all_nodes)

        #	node_list = all_nodes
        #	if node_list is None:
        #		node_list = []

        #	for node in node_list:
        #		if node.name.startswith('slave'):
        #		    del_nodes.append(node)
        #		    break
	
        node_obj = gce.ex_get_node(node_name)
        del_nodes.append(node_obj)
        logger.info(' To Delete Node: %s' % del_nodes)

        result = gce.ex_destroy_multiple_nodes(del_nodes)
        for i, success in enumerate(result):
            if success:
                logger.info(' Deleted %s' % del_nodes[i].name)
            else:
                logger.info(' Failed to delete %s' % del_nodes[i].name)

        end_time = datetime.datetime.now()
        logger.info(' Total runtime Delete Node: %s' % str(end_time - start_time))

    def get_image(self, name):
        gce = self._get_connection()
        
        return gce.ex_get_image(name)

    def get_size(self, size):
        gce = self._get_connection()

        return gce.ex_get_size(size)

    def scale_up(self, size, image, snapshot):
        gce = self._get_connection()

        #Start the timer
        start_time = datetime.datetime.now()
        logger.info(' Compute Create Node start time: %s' % str(start_time))

        #Create the boot disk from snapshot
        logger.info(' Creating disk for node from snapshot: %s' % snapshot)

        ts = int(time.time())
        name = 'slave-%s' % ts

        volume = gce.create_volume(None, name, snapshot=snapshot)
        logger.debug(' Created Disk: %s from snapshot' % name)
	
	#Set Access to Cumpute and Storage API Access for Slaves
	sa_scopes = [{'scopes': ['compute', 'storage-full']}]

        # Create Node with Disk
        node_2 = gce.create_node(name, size, image, ex_tags=['mesos-slave'],
                         ex_boot_disk=volume,
                         ex_disk_auto_delete=True,
			 ex_service_accounts=sa_scopes)

        logger.info(' Node %s created' % name)

        end_time = datetime.datetime.now()
        logger.info(' Total runtime Create Node: %s' % str(end_time - start_time))

