#!/usr/bin/env python
import argparse
import logging
import sys
import time

from autoscale import MesosReporter, MesosDecider, MesosSlaveReporter, MesosSlaveDecider, GceScaleService

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--log-level', default="warn", help='Log level (debug, [default] info, warn, error)')
    parser.add_argument('-u', '--mesos-url', help='Mesos cluster URL', required=True)
    parser.add_argument('-c', '--cpus', help='Comma-delimited CPU thresholds (lower,upper)')    
    parser.add_argument('-d', '--disk', help='Comma-delimited disk thresholds (lower,upper)')
    parser.add_argument('-m', '--mem', help='Comma-delimited memory thresholds (lower,upper)')
    parser.add_argument('-p', '--project', help='GCE Project Id', required=True)
    parser.add_argument('-r', '--region', help='GCE region')
    parser.add_argument('-a', '--account', help='GCE Service Account')
    parser.add_argument('-k', '--key', help='GCE Pem Key')

    args = parser.parse_args()

    logger = logging.getLogger(__name__)
    logging.basicConfig(stream=sys.stderr, level=getattr(logging, args.log_level.upper()))

    thresholds = {}
    if args.cpus:
        lower, upper = args.cpus.split(',')
        thresholds['cpus'] = dict(lower=int(lower), upper=int(upper))
    if args.disk:
        lower, upper = args.disk.split(',')
        thresholds['disk'] = dict(lower=int(lower), upper=int(upper))
    if args.mem:
        lower, upper = args.mem.split(',')
        thresholds['mem'] = dict(lower=int(lower), upper=int(upper))

    gce = GceScaleService(args.project, args.region, args.account, args.key)

    while(True):

	reporter = MesosReporter(args.mesos_url)
	decider = MesosDecider(thresholds)

	delta = decider.should_scale(reporter)
	if delta < 0:
		print('Scaling in {region} by {delta}'.format(region=args.region, delta=delta))
		#Get the slave to remove
		slave_reporter = MesosSlaveReporter(args.mesos_url)
		slave_decider = MesosSlaveDecider()
		slave_info = slave_decider.slave_info(slave_reporter)

		gce.scale_down(slave_info)
		time.sleep(90)
	elif delta > 0:
		print('Skipping scaling in {region}  by {delta}'.format(region=args.region, delta=delta))
		time.sleep(10)

	else:
		print('No change needed in {region}'.format(region=args.region))
		time.sleep(10)


if __name__ == '__main__':
    main()
