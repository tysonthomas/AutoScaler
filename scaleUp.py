#!/usr/bin/env python
import argparse
import logging
import sys
import datetime
import time

from autoscale import MesosReporter, MesosDecider, GceScaleService

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--log-level', default="warn", help='Log level (debug, [default] info, warn, error)')
    parser.add_argument('-u', '--mesos-url', help='Mesos cluster URL', required=True)
    parser.add_argument('-c', '--cpus', help='Comma-delimited CPU thresholds (lower,upper)')    
    parser.add_argument('-d', '--disk', help='Comma-delimited disk thresholds (lower,upper)')
    parser.add_argument('-m', '--mem', help='Comma-delimited memory thresholds (lower,upper)')
    parser.add_argument('-p', '--project', help='GCE Project Id', required=True)
    parser.add_argument('-r', '--region', help='GCE region')
    parser.add_argument('-a', '--account', help='GCE Service Account')
    parser.add_argument('-k', '--key', help='GCE Pem Key')

    args = parser.parse_args()

    logger = logging.getLogger(__name__)
    logging.basicConfig(stream=sys.stderr, level=getattr(logging, args.log_level.upper()))

    thresholds = {}
    if args.cpus:
        lower, upper = args.cpus.split(',')
        thresholds['cpus'] = dict(lower=int(lower), upper=int(upper))
    if args.disk:
        lower, upper = args.disk.split(',')
        thresholds['disk'] = dict(lower=int(lower), upper=int(upper))
    if args.mem:
        lower, upper = args.mem.split(',')
        thresholds['mem'] = dict(lower=int(lower), upper=int(upper))

    gce = GceScaleService(args.project, args.region, args.account, args.key)
    # Get latest Debian 7 image
    image = gce.get_image('centos-7')
    # Get Machine Size
    size = gce.get_size('n1-standard-1')
    # Get Sanpshot name
    snapshot = 'node-1'

    while(True):

	reporter = MesosReporter(args.mesos_url)
	decider = MesosDecider(thresholds)

	delta = decider.should_scale(reporter)
	if delta > 0:
		print('Scaling in {region} by {delta}'.format(region=args.region, delta=delta))
		gce.scale_up(size, image, snapshot)
		time.sleep(30)
	elif delta < 0:
		print('Not scaling down in {region}'.format(region=args.region))
		time.sleep(10)
	else:
		print('No change needed in {region}'.format(region=args.region))
		time.sleep(10)


if __name__ == '__main__':
    main()
