FROM ubuntu:14.04
MAINTAINER tyson.thomas@caspen.co.in

WORKDIR /home/autoscale

RUN apt-get update && apt-get install -y python-pip
RUN pip install apache-libcloud

ADD . /home/autoscale

RUN chmod +x /home/autoscale/scaleUp.py

CMD ["scaleUp.py"]

