This is a autoscale program that scales up or scales down the number of instance in the Google Compute Engine Cloud.

The steps required to setup the environment.
    1.Create a 'mesos-slave' snapshot image that can connect to the 'mesos-master' on system boot.
    2.Input the snapshot details in the python scripts -> scaleUp.py
    3.Set the input values for the type of system and the region to run.
    4.Install libcloud using pip on the host machine.
    5.Setup the credentials for authenticating the libclouod class.
        Note:For running the script in the gce instance itself there is option of starting the instance with auto access to the credentials, so only project-id is required.
    6.Run the independent python scripts for scaling up and scaling down the server instances.
    
    Commands:
    
    python scaleDown.py -u http://130.211.252.146:5050 --log-level info --cpus 1,2 -p api-project-188888845848 --region us-central1-f -a 188888845848-b523sgo0mf7k4ntq9r46qq28i47jc1ft@developer.gserviceaccount.com -k /home/user/.ssh/google_compute.json

    python mesos.py -u http://130.211.252.146:5050 --log-level info --cpus 1,2 -p api-project-188888845848 --region us-central1-f 
    
    
    Here 
        -u                  is the mesos-master cluster leader ip
        -cpus lower, upper -bounds for scaling
        -p                  project-id
        --region            destination region to start the systems
        -a                 The Service Account Id
        -k                  The pem or json oauth key
        